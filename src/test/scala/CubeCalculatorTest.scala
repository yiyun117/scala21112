import org.scalatest.FunSuite

class CubeCalculatorTest extends FunSuite {
  test("CubeCalculator.cube") {
    assert(exercise.CubeCalculator.cube(3) === 27)
    assert(exercise.CubeCalculator.cube(0) === 0)
  }
}