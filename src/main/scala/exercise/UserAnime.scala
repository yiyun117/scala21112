package exercise

import org.apache.spark.sql.functions._
import org.apache.spark.sql.SparkSession

object UserAnime {
  def main(args: Array[String]) {
    val spark = SparkSession.builder
      .master("local[*]")
      .appName("Spark Scala Exercise")
      .getOrCreate()

    val df = spark.read
      .format("csv")
      .option("header", "true") //first line in file has headers \
      .option("mode", "DROPMALFORMED")
      .load("src/main/resources/useranime1000.csv")
    df.printSchema()

    val distinctUser = df.select(df("username")).distinct
    println(distinctUser.show())

    //save the result of aggregation grouped by username to a file
    val AggDF = df.groupBy("username").agg(countDistinct("anime_id") as "TotalWatchedAnimes",
    sum("my_watched_episodes") as "TotalWatchedEpisodes",
      avg("my_score") as "avgScore")
    AggDF.write.format("csv").save("src/main/resources/aggregation.csv")
  }
}
