package exercise
import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.sql.SparkSession

object MyfirstSparkScala {
  def main(args: Array[String]) {
    val spark = SparkSession.builder
      .master("local[2]")
      .appName("Spark SQL basic example")
      .getOrCreate()

    // For implicit conversions like converting RDDs to DataFrames
    import spark.implicits._
    val df = spark.read
      .format("csv")
      .option("header", "true") //first line in file has headers
      .option("mode", "DROPMALFORMED")
      .load("src/main/resources/useranime1000.csv")
    df.printSchema()
  }
}
